const plugin = require('tailwindcss/plugin')
const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  content: ["./src/app/**/*.{html,ts}"],
  theme: {
    extend: {
      colors: {
        'branch': '#826b57'
      }
    },
  },
  daisyui: {
    themes: ['light'],
  },
  plugins: [
    require('@tailwindcss/forms'), require('@tailwindcss/typography'), require('daisyui'),

    plugin(function ({ addBase, theme }) {
      addBase({
        'h1': { fontSize: '54px', fontWeight: '700' },
        'h2': { fontSize: '40px', fontWeight: '700' },
        'h3': { fontSize: '28px', fontWeight: '500', color: '#826b57' },
        'h4': { fontSize: '24px', fontWeight: '500', color: '#826b57' },
        'h5': { fontSize: '20px', fontWeight: '500', color: '#826b57' },
        'p': { fontSize: '18px', fontWeight: '400', color: 'rgba(50, 50, 50,0.8)' }
      })
    }),
  ],
}
