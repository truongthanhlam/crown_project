import { Component } from '@angular/core';

@Component({
    selector: 'app-warranty-2',
    standalone: true,
    imports: [],
    templateUrl: './warranty-2.component.html',
    styleUrl: './warranty-2.component.scss',
})
export default class Warranty2Component {}
