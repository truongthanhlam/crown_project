import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Warranty2Component } from './warranty-2.component';

describe('Warranty2Component', () => {
  let component: Warranty2Component;
  let fixture: ComponentFixture<Warranty2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [Warranty2Component]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Warranty2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
