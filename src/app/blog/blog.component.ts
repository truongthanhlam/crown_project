import { Component, inject } from '@angular/core';
import { toSignal } from '@angular/core/rxjs-interop';
import { BlogService } from '../data-access/services/blog.service';
import { map } from 'rxjs';
import { BlogModel } from '../data-access/models/blog.model';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'app-blog',
    standalone: true,
    imports: [],
    templateUrl: './blog.component.html',
    styleUrl: './blog.component.scss',
})
export default class BlogComponent {
    router = inject(Router);
    route = inject(ActivatedRoute);
    blogService = inject(BlogService);

    blogList = toSignal(
        this.blogService.getItems().pipe(
            map((list) => {
                return list.map((detail) => {
                    return {
                        ...detail,
                        brief: this.extractFirstParagraph(detail.content),
                    };
                });
            })
        )
    );

    blogOnClick(blog: BlogModel) {
        this.router.navigate([blog.slug], { relativeTo: this.route });
    }

    extractFirstParagraph(htmlContent: string): string | null {
        const parser = new DOMParser();
        const doc = parser.parseFromString(htmlContent, 'text/html'); // Parse the HTML string

        const firstParagraph = doc.querySelector('p'); // Select the first <p> element
        return firstParagraph ? firstParagraph.textContent : null; // Return the content of the first <p> or null if not found
    }
}
