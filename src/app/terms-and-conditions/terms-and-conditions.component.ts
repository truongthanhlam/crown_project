import { Component, signal } from '@angular/core';
import {
    Term,
    TermsAndCondition,
} from '../data-access/models/terms-and-conditions.model';

@Component({
    selector: 'app-terms-and-conditions',
    standalone: true,
    imports: [],
    templateUrl: './terms-and-conditions.component.html',
    styleUrl: './terms-and-conditions.component.scss',
})
export default class TermsAndConditionsComponent {
    terms = signal<Term[]>(TermsAndCondition);
}
