import { Component, inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DisplayedProductListService } from '../data-access/services/displayed-product-list.service';
import { toSignal } from '@angular/core/rxjs-interop';
import { UpperCasePipe } from '@angular/common';

@Component({
    selector: 'app-home',
    standalone: true,
    imports: [UpperCasePipe],
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
})
export default class HomeComponent implements OnInit {
    displayedProductListService = inject(DisplayedProductListService);
    router = inject(Router);
    displayedProductList = toSignal(
        this.displayedProductListService.getItems()
    );

    constructor() {}

    ngOnInit(): void {}

    navigate(path: string) {
        this.router.navigate([path]);
    }
}
