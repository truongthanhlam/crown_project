import {
    ApplicationConfig,
    importProvidersFrom,
    provideZoneChangeDetection,
} from '@angular/core';
import { provideRouter, RouterModule } from '@angular/router';

import { registerLocaleData } from '@angular/common';
import vi from '@angular/common/locales/vi';
import { FormsModule } from '@angular/forms';
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';
import { provideHttpClient } from '@angular/common/http';
import { initializeApp, provideFirebaseApp } from '@angular/fire/app';
import { getAuth, provideAuth } from '@angular/fire/auth';
import { getFirestore, provideFirestore } from '@angular/fire/firestore';
import { getStorage, provideStorage } from '@angular/fire/storage';
import { FIREBASE_OPTIONS } from '@angular/fire/compat';
import { routes } from './app.routes';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

registerLocaleData(vi);

export const appConfig: ApplicationConfig = {
    providers: [
        importProvidersFrom(
            RouterModule.forRoot(routes, {
                scrollPositionRestoration: 'enabled',
            }),
            BrowserAnimationsModule
        ),
        provideZoneChangeDetection({ eventCoalescing: true }),
        provideFirebaseApp(() =>
            initializeApp({
                apiKey: 'AIzaSyDBaIsuVb5JZDfx2Rghgo85t7OQ73zjMjg',
                authDomain: 'website-cms-panel.firebaseapp.com',
                projectId: 'website-cms-panel',
                storageBucket: 'website-cms-panel.appspot.com',
                messagingSenderId: '602484632099',
                appId: '1:602484632099:web:09b5e19be487124d47df96',
                measurementId: 'G-305H046RSG',
            })
        ),
        provideFirestore(() => getFirestore()),
    ],
};
