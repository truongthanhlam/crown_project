import { Component } from '@angular/core';

@Component({
    selector: 'app-membership',
    standalone: true,
    imports: [],
    templateUrl: './membership.component.html',
    styleUrl: './membership.component.scss',
})
export default class MembershipComponent {}
