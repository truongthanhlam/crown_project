import { Component, inject } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-warranty-home',
    standalone: true,
    imports: [],
    templateUrl: './warranty-home.component.html',
    styleUrl: './warranty-home.component.scss',
})
export default class WarrantyHomeComponent {
    router = inject(Router);

    navigate(path: string) {
        this.router.navigate([path]);
    }
}
