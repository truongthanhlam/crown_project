import { Component, inject, Signal } from '@angular/core';
import { BlogService } from '../data-access/services/blog.service';
import { toSignal } from '@angular/core/rxjs-interop';
import { BlogModel } from '../data-access/models/blog.model';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs';

@Component({
    selector: 'app-blog-detail',
    standalone: true,
    imports: [],
    templateUrl: './blog-detail.component.html',
    styleUrl: './blog-detail.component.scss',
})
export default class BlogDetailComponent {
    route = inject(ActivatedRoute);

    blogService = inject(BlogService);
    blogDetail: Signal<BlogModel | undefined>;

    constructor() {
        this.blogDetail = toSignal(
            this.route.params.pipe(
                switchMap((params) => {
                    return this.blogService.getBySlug(params['slug']);
                })
            )
        );
    }
}
