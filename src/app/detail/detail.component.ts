import { CurrencyPipe, NgClass } from '@angular/common';
import {
    Component,
    computed,
    effect,
    inject,
    OnInit,
    Signal,
} from '@angular/core';
import { ImageZoomPreviewDirective } from '../directives/image-zoom-preview.directive';
import { toSignal, toObservable } from '@angular/core/rxjs-interop';
import { switchMap, of } from 'rxjs';
import { DisplayedProductListModel } from '../data-access/models/displayed-product-list.model';
import { ProductModel } from '../data-access/models/product.model';
import { ActivatedRoute } from '@angular/router';
import { DisplayedProductListService } from '../data-access/services/displayed-product-list.service';
import { ProductService } from '../data-access/services/product.service';
import {
    FormBuilder,
    FormGroup,
    FormsModule,
    ReactiveFormsModule,
} from '@angular/forms';
import { DataListWrapperComponent } from '../@shared/components/data-list-wrapper/data-list-wrapper.component';
import { SelectComponent } from '../@shared/components/select/select.component';
import { categoryStringify } from '../data-access/models/category.model';
import { isEqual } from 'lodash-es';
const IMAGES = [
    'https://crownhanoi.com/wp-content/uploads/2021/09/BD14302156_6_PRONG_SOLITAIRE_PAVE_RING__BORDER_RING.jpg',
    'https://crownhanoi.com/wp-content/uploads/2021/09/BD14302156_6_PRONG_SOLITAIRE_PAVE_RING__BORDER_RING-1.jpg',
    'https://crownhanoi.com/wp-content/uploads/2021/09/BD14302156_6_PRONG_SOLITAIRE_PAVE_RING__BORDER_RING-2.jpg',
    'https://crownhanoi.com/wp-content/uploads/2021/09/BD14302156_6_PRONG_SOLITAIRE_PAVE_RING__BORDER_RING-3.jpg',
];
@Component({
    selector: 'app-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss'],
    imports: [
        NgClass,
        ImageZoomPreviewDirective,
        FormsModule,
        ReactiveFormsModule,
        DataListWrapperComponent,
        SelectComponent,
        CurrencyPipe,
    ],
    standalone: true,
})
export default class DetailComponent implements OnInit {
    selectedIndex = 0;
    route = inject(ActivatedRoute);
    fb = inject(FormBuilder);

    displayedProductListService = inject(DisplayedProductListService);
    productService = inject(ProductService);

    listDetail: Signal<DisplayedProductListModel | undefined>;

    categoryStringify = categoryStringify;

    productDetail: Signal<ProductModel | undefined>;
    images: Signal<string[] | undefined> = computed(() => {
        return this.productDetail()?.photos || [];
    });

    selectionForm = new FormGroup({ selection: new FormGroup({}) });
    get selectionGroup() {
        return this.selectionForm.get('selection') as FormGroup;
    }
    selectionFormChange: Signal<any> = toSignal(
        this.selectionForm.valueChanges
    );

    price = computed(() => {
        if (this.productDetail() && this.selectionFormChange()) {
            const variation = this.productDetail()!.variation;

            const selectedIds = Object.values(
                this.selectionFormChange().selection
            ).map((item: any) => item?.id);
            const item = variation.find((item) =>
                isEqual(item.ids, selectedIds)
            );
            if (!item) {
                return -1;
            }
            return item.price;
        }
        return -1;
    });

    constructor() {
        this.listDetail = toSignal(
            this.route.params.pipe(
                switchMap(({ type }) => {
                    return this.displayedProductListService.getBySlug(type);
                })
            )
        );
        this.productDetail = toSignal(
            this.route.params.pipe(
                switchMap(({ slug }) => {
                    return this.productService.getItemById(slug);
                })
            )
        );

        effect(() => {
            if (this.productDetail()) {
                let obj: any = {};
                this.productDetail()?.attribute.forEach((att) => {
                    obj[att.id!] = this.fb.control(null);
                });
                this.selectionForm.setControl('selection', this.fb.group(obj));
            }
        });
    }

    ngOnInit(): void {}

    selectImage(index: number) {
        this.selectedIndex = index;
    }
}
