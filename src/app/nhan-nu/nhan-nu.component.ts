import {
    ChangeDetectionStrategy,
    Component,
    ViewChild,
    OnInit,
    inject,
    effect,
    Signal,
    signal,
} from '@angular/core';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import { DisplayedProductListService } from '../data-access/services/displayed-product-list.service';
import { ProductService } from '../data-access/services/product.service';
import { toObservable, toSignal } from '@angular/core/rxjs-interop';
import { catchError, of, switchMap } from 'rxjs';
import { DisplayedProductListModel } from '../data-access/models/displayed-product-list.model';
import { ProductModel } from '../data-access/models/product.model';

export interface Image {
    name: string;
    imageUrl: string;
    price: string;
    url: string;
}

@Component({
    selector: 'app-nhan-nu',
    templateUrl: './nhan-nu.component.html',
    styleUrls: ['./nhan-nu.component.scss'],
    imports: [RouterLink],
    standalone: true,
})
export default class NhanNuComponent implements OnInit {
    displayedProductListService = inject(DisplayedProductListService);
    productService = inject(ProductService);
    router = inject(Router);
    route = inject(ActivatedRoute);

    listDetail: Signal<DisplayedProductListModel | undefined>;

    productList: Signal<ProductModel[] | undefined>;
    constructor() {
        this.listDetail = toSignal(
            this.route.params.pipe(
                switchMap(({ type }) => {
                    return this.displayedProductListService.getBySlug(type);
                })
            )
        );
        this.productList = toSignal(
            toObservable(this.listDetail).pipe(
                switchMap((detail) => {
                    if (!detail) {
                        return of([]);
                    }
                    return this.productService.getItemByIds(detail!.productIds);
                })
            )
        );
        // effect(() => {
        //     if (!this.listDetail()) {
        //         this.router.navigate(['/home']);
        //     }
        // });
    }

    ngOnInit(): void {}

    readonly items = [
        'Thứ tự mặc định',
        'Thứ tự theo mức độ phổ biến',
        'Mới nhất',
        'Thứ tự theo giá: thấp đến cao',
        'Thứ tự theo giá: cao xuống thấp',
    ];

    images: Image[] = [
        {
            name: '6 PRONG SOLITAIRE PAVE RING & BORDER RING',
            imageUrl:
                'https://crownhanoi.com/wp-content/uploads/2021/09/BD14302156_6_PRONG_SOLITAIRE_PAVE_RING__BORDER_RING-768x768.jpg',
            price: '13,620,000',
            url: '6-prong-solitaire-pave-ring-border-ring/',
        },
        {
            name: '6 PRONG SOLITAIRE PAVE RING & BORDER RING',
            imageUrl:
                'https://crownhanoi.com/wp-content/uploads/2021/01/BD1430184_6_PRONG_SOLITAIRE_RING__BAND_RING__3MM-768x768.jpg',
            price: '12,000,000',
            url: '6-prong-solitaire-ring-band-ring-3mm/',
        },
        {
            name: 'BAGUETTE BAND COUPLE RING',
            imageUrl:
                'https://crownhanoi.com/wp-content/uploads/2020/10/BD14212_BAGUETTE_BAND_COUPLE_RING-768x768.jpg',
            price: '13,620,000',
            url: 'baguette-band-couple-ring/',
        },
        {
            name: '6 PRONG SOLITAIRE PAVE RING & BORDER RING',
            imageUrl:
                'https://crownhanoi.com/wp-content/uploads/2021/09/BD14302156_6_PRONG_SOLITAIRE_PAVE_RING__BORDER_RING-768x768.jpg',
            price: '13,620,000',
            url: '6-prong-solitaire-pave-ring-border-ring/',
        },
        {
            name: '6 PRONG SOLITAIRE PAVE RING & BORDER RING',
            imageUrl:
                'https://crownhanoi.com/wp-content/uploads/2021/01/BD1430184_6_PRONG_SOLITAIRE_RING__BAND_RING__3MM-768x768.jpg',
            price: '12,000,000',
            url: '6-prong-solitaire-ring-band-ring-3mm/',
        },
        {
            name: 'BAGUETTE BAND COUPLE RING',
            imageUrl:
                'https://crownhanoi.com/wp-content/uploads/2020/10/BD14212_BAGUETTE_BAND_COUPLE_RING-768x768.jpg',
            price: '13,620,000',
            url: 'baguette-band-couple-ring/',
        },
    ];

    open = false;

    itemOnClick(product: ProductModel) {
        this.router.navigate([product.id!], { relativeTo: this.route });
    }
}
