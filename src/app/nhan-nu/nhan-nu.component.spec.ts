import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NhanNuComponent } from './nhan-nu.component';

describe('NhanNuComponent', () => {
  let component: NhanNuComponent;
  let fixture: ComponentFixture<NhanNuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NhanNuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NhanNuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
