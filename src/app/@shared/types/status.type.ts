export type StatusType = 'error' | 'info' | 'warning' | 'success' | 'normal';
