export function formatCurrency(value: string): string {
    const numberValue = parseInt(value, 10);
    if (isNaN(numberValue) || !numberValue) {
        return '';
    }

    return numberValue.toLocaleString('vi-VN', {
        style: 'currency',
        currency: 'VND',
    });
}

export function stringToSlug(str: string): string {
    return str
        .toLowerCase() // Convert to lowercase
        .normalize('NFD') // Normalize to decompose special characters
        .replace(/[\u0300-\u036f]/g, '') // Remove accents
        .replace(/[^a-z0-9\s-]/g, '') // Remove special characters
        .trim() // Trim whitespace
        .replace(/\s+/g, '-') // Replace spaces with hyphens
        .replace(/-+/g, '-');
}
