import {
    Directive,
    ElementRef,
    HostListener,
    ViewContainerRef,
    input,
} from '@angular/core';
import { stringToSlug } from '../util/string.util';

@Directive({
    selector: '[appSlugMask]',
    standalone: true,
})
export class SlugMaskDirective {
    appSlugMask = input<boolean>(true);
    private lastValue: string = '';

    constructor(private el: ElementRef) {}

    @HostListener('input', ['$event']) onInputChange(event: Event) {
        if (!this.appSlugMask()) {
            return;
        }

        const input = this.el.nativeElement;
        const value = input.value.trim();
        if (!value && input.value) {
            input.value = value;
            return;
        }
        const formattedValue = stringToSlug(value);
        if (this.lastValue !== formattedValue) {
            input.value = formattedValue.trim();
            this.lastValue = formattedValue;
        }
    }
}
