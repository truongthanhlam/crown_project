import {
    Directive,
    ElementRef,
    HostListener,
    ViewContainerRef,
    input,
} from '@angular/core';
import { formatCurrency } from '../util/string.util';

@Directive({
    selector: '[appCurrencyMask]',
    standalone: true,
})
export class CurrencyMaskDirective {
    appCurrencyMask = input<boolean>(true);
    private lastValue: string = '';

    constructor(private el: ElementRef) {}

    @HostListener('input', ['$event']) onInputChange(event: Event) {
        if (!this.appCurrencyMask()) {
            return;
        }

        const input = this.el.nativeElement;
        const value = input.value.trim().replace(/[^0-9]/g, '');
        if (!value && input.value) {
            input.value = value;
            return;
        }
        const formattedValue = formatCurrency(value);
        if (this.lastValue !== formattedValue) {
            input.value = formattedValue.trim();
            this.lastValue = formattedValue;
        }
    }
}
