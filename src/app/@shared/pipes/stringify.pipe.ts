import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'appStringify',
    standalone: true,
})
export class StringifyPipe implements PipeTransform {
    transform(value: any, stringifyFn: (item: any) => string): unknown {
        if (!stringifyFn) {
            return JSON.stringify(value);
        }
        return stringifyFn(value) || '';
    }
}
