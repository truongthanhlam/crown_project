import { Component, computed, input, output } from '@angular/core';
import { NgClass } from '@angular/common';
import { NgIcon, provideIcons } from '@ng-icons/core';
import { matClose } from '@ng-icons/material-icons/baseline';
import { StatusType } from '../../types/status.type';

@Component({
    selector: 'app-tag',
    standalone: true,
    imports: [NgClass, NgIcon],
    templateUrl: './tag.component.html',
    styleUrl: './tag.component.scss',
    providers: [provideIcons({ matClose })],
})
export class TagComponent {
    type = input<StatusType>('normal');
    onClose = output<void>();
    color = computed(() => {
        switch (this.type()) {
            case 'error':
                return 'bg-error';
            case 'info':
                return 'bg-info';
            case 'success':
                return 'bg-success';
            case 'warning':
                return 'bg-warning';
            default:
                return 'bg-base-300';
        }
    });
}
