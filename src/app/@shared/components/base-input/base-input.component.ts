import {
    AfterViewChecked,
    AfterViewInit,
    Component,
    ElementRef,
    forwardRef,
    inject,
    input,
    OnInit,
    Optional,
    output,
    Provider,
    Self,
    signal,
    viewChild,
} from '@angular/core';
import {
    ControlValueAccessor,
    NG_VALUE_ACCESSOR,
    NgControl,
} from '@angular/forms';
import { v4 as uuid } from 'uuid';

const INPUT_VALUE_ACCESSOR: Provider = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => BaseInputComponent),
    multi: true,
};

@Component({
    selector: 'app-base-input',
    standalone: true,
    imports: [],
    templateUrl: './base-input.component.html',
    styleUrl: './base-input.component.scss',
    providers: [INPUT_VALUE_ACCESSOR],
})
export class BaseInputComponent
    implements ControlValueAccessor, AfterViewChecked
{
    inputElement = viewChild<ElementRef>('input');
    label = input<string>();
    placeholder = input<string>('');
    internalValue = signal<any>(null);
    focused = signal<boolean>(false);
    readOnly = input<boolean>(false);
    disabled = signal<boolean>(false);
    autofocus = input(false);
    onChange: any = () => {};
    onTouched: any = () => {};
    ngControl = inject(NgControl);
    id = uuid();
    constructor() {
        if (this.ngControl != null) {
            this.ngControl.valueAccessor = this;
        }
    }

    ngAfterViewChecked(): void {
        if (this.autofocus()) {
            this.inputElement()?.nativeElement.focus();
        }
    }

    writeValue(value: any): void {
        this.internalValue.set(value || '');
    }
    registerOnChange(fn: any): void {
        this.onChange = fn;
    }
    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }
    setDisabledState?(isDisabled: boolean): void {
        this.disabled.set(isDisabled);
    }

    onInputChange(value: any): void {
        if (value instanceof Event) {
            const inputElement = value.target as HTMLInputElement;

            this.internalValue.set(+inputElement.value || inputElement.value);
        } else {
            this.internalValue.set(value);
        }
        this.onChange(this.internalValue());
    }

    onBlur(): void {
        this.onTouched();
    }

    get isError() {
        return this.ngControl.errors && this.ngControl.touched;
    }

    get isDisabled(): boolean {
        return this.ngControl.disabled!;
    }
}
