import { CommonModule, JsonPipe, NgClass } from '@angular/common';
import {
    Component,
    forwardRef,
    input,
    model,
    OnInit,
    Provider,
    signal,
} from '@angular/core';
import {
    ControlValueAccessor,
    FormsModule,
    NG_VALUE_ACCESSOR,
} from '@angular/forms';
import { CurrencyMaskDirective } from '../../directives/currency-mask.directive';
import { SlugMaskDirective } from '../../directives/slug-mask.directive';
import { BaseInputComponent } from '../base-input/base-input.component';
import { formatCurrency } from '../../util/string.util';
@Component({
    selector: 'app-input',
    standalone: true,
    imports: [
        FormsModule,
        JsonPipe,
        CurrencyMaskDirective,
        NgClass,
        SlugMaskDirective,
    ],
    templateUrl: './input.component.html',
    styleUrl: './input.component.scss',
})
export class InputComponent extends BaseInputComponent {
    type = input<string>();
    mask = input<null | 'currency' | 'slug'>();
    override onInputChange(value: Event): void {
        if (value instanceof Event) {
            const inputElement = value.target as HTMLInputElement;
            this.internalValue.set(+inputElement.value || inputElement.value);
        }
        if (this.mask() === 'currency') {
            this.onChange(this.parseCurrencyToNumber(this.internalValue()));
            return;
        }
        this.onChange(this.internalValue());
    }

    override writeValue(value: any): void {
        if (this.mask() === 'currency' && value) {
            this.internalValue.set(formatCurrency(value));
            return;
        }
        this.internalValue.set(value || '');
    }

    parseCurrencyToNumber(currencyValue: string): number {
        const numericValue = currencyValue.toString().replace(/[^\d]/g, '');
        return parseFloat(numericValue);
    }
}
