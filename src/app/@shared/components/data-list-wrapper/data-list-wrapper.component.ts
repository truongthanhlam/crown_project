import {
    Component,
    contentChild,
    effect,
    input,
    OnInit,
    output,
    signal,
    TemplateRef,
    viewChild,
} from '@angular/core';
import { OptionsDirective } from '../select/options.directive';
import { OptionDirective } from '../select/option.directive';
import { InputComponent } from '../input/input.component';
import {
    FormControl,
    FormControlDirective,
    FormsModule,
    ReactiveFormsModule,
} from '@angular/forms';
import { JsonPipe, NgTemplateOutlet } from '@angular/common';
import { NgIcon, provideIcons } from '@ng-icons/core';
import { matCheck } from '@ng-icons/material-icons/baseline';
import { toSignal } from '@angular/core/rxjs-interop';
import { debounceTime, map } from 'rxjs';
import { SelectComponent } from '../select/select.component';

@Component({
    selector: 'app-data-list-wrapper',
    standalone: true,
    imports: [
        OptionsDirective,
        OptionDirective,
        InputComponent,
        FormsModule,
        ReactiveFormsModule,
        NgTemplateOutlet,
        JsonPipe,
        NgIcon,
    ],
    templateUrl: './data-list-wrapper.component.html',
    styleUrl: './data-list-wrapper.component.scss',
    providers: [provideIcons({ matCheck })],
})
export class DataListWrapperComponent<T> {
    optionsDirective = viewChild<OptionsDirective>(OptionsDirective);
    selectComponent = signal<SelectComponent | null>(null);
    data = input<T[] | undefined | null>([]);
    enableSearch = input<boolean>(false);
    stringify = input((item: T) => item as string);
    displayOptionTemplate = input<TemplateRef<any>>();
    requireSelect = input<boolean>(false);
    searchInputControl = new FormControl('');
    searchInputChange = toSignal<String>(
        this.searchInputControl.valueChanges.pipe(
            debounceTime(500),
            map((value) => value ?? '')
        )
    );
    searchChange = output<String>();

    constructor() {
        effect(() => {
            this.searchChange.emit(this.searchInputChange()!);
        });

        effect(
            () => {
                if (
                    this.selectComponent() &&
                    !this.selectComponent()?.dropdownOpen()
                ) {
                    this.searchInputControl.patchValue('');
                }
            },
            { allowSignalWrites: true }
        );
    }
    register(select: SelectComponent) {
        this.selectComponent.set(select);
    }
}
