import {
    Directive,
    ElementRef,
    HostListener,
    Renderer2,
    effect,
    input,
    signal,
} from '@angular/core';
import { OptionsDirective } from './options.directive';

@Directive({
    selector: '[appOption]',
    standalone: true,
})
export class OptionDirective {
    value = input<any>(null);
    activeClass = input<string>();
    isSelected = signal<boolean>(false);
    private optionsDirective!: OptionsDirective;

    constructor(private el: ElementRef, private renderer: Renderer2) {
        this.renderer.addClass(this.el.nativeElement, 'hover:bg-gray-100');
        effect(() => {
            this.updateClass();
        });
    }

    registerWithOptions(optionsDirective: OptionsDirective) {
        this.optionsDirective = optionsDirective;
    }

    @HostListener('click')
    onClick(): void {
        this.optionsDirective.selectOption(this);
    }

    updateClass() {
        if (this.isSelected()) {
            this.renderer.addClass(
                this.el.nativeElement,
                ['is-selected', this.activeClass()].join(' ').trim()
            );
        } else {
            this.renderer.removeClass(
                this.el.nativeElement,
                ['is-selected', this.activeClass()].join(' ').trim()
            );
        }
    }
}
