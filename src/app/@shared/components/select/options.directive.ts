import { map } from 'rxjs';
import {
    AfterContentInit,
    Directive,
    QueryList,
    computed,
    contentChildren,
    effect,
    input,
    output,
    signal,
} from '@angular/core';
import { OptionDirective } from './option.directive';
import { SelectComponent } from './select.component';

@Directive({
    selector: '[appOptions]',
    standalone: true,
})
export class OptionsDirective implements AfterContentInit {
    options = contentChildren<OptionDirective>(OptionDirective);
    selectedOptionChange = output<any>();
    selectedOptions = signal<any[]>([]);

    stringify = input<(item: any) => string>((item: any) => item);
    select = signal<SelectComponent | null>(null);

    constructor() {
        effect(
            () => {
                this.options().forEach((option) => {
                    option.registerWithOptions(this);
                });
            },
            { allowSignalWrites: true }
        );

        effect(
            () => {
                this.options().forEach((opt) =>
                    opt.isSelected.set(
                        this.selectedOptions().includes(opt.value())
                    )
                );
            },
            { allowSignalWrites: true }
        );
    }
    ngAfterContentInit(): void {}
    selectOption(option: OptionDirective) {
        if (this.select()?.multiple()) {
            this.selectedOptions.update((old): any[] => {
                if (old.includes(option.value())) {
                    return old.filter((item) => item !== option.value());
                }
                return [...old, option.value()];
            });
        }

        if (!this.select()?.multiple()) {
            this.selectedOptions.set([option.value()]);
        }
        this.selectedOptionChange.emit(this.selectedOptions());
    }

    writeSelectedOption(values: any[]) {
        this.selectedOptions.set(values);
    }

    register(select: SelectComponent) {
        this.select.set(select);
    }
    removeOption(remover: any) {
        this.selectedOptions.update((old) =>
            old.filter((item) => item !== remover)
        );
    }
}
