import {
    AfterContentInit,
    Component,
    HostListener,
    Injector,
    computed,
    contentChild,
    contentChildren,
    effect,
    inject,
    input,
    signal,
    viewChildren,
} from '@angular/core';
import { NgClass, NgIf } from '@angular/common';
import { SelectService } from './select.service';
import { OptionsDirective } from './options.directive';
import { TagComponent } from '../tag/tag.component';
import { OptionDirective } from './option.directive';
import { DataListWrapperComponent } from '../data-list-wrapper/data-list-wrapper.component';
import { trigger, transition, style, animate } from '@angular/animations';
import { NgIconComponent, provideIcons } from '@ng-icons/core';
import { matExpandMore } from '@ng-icons/material-icons/baseline';
import { StringifyPipe } from '../../pipes/stringify.pipe';
import { BaseInputComponent } from '../base-input/base-input.component';

@Component({
    selector: 'app-select',
    standalone: true,
    imports: [NgIconComponent, NgIf, NgClass, StringifyPipe, TagComponent],
    templateUrl: './select.component.html',
    styleUrl: './select.component.scss',
    providers: [provideIcons({ matExpandMore })],
    animations: [
        trigger('popInOut', [
            transition(':enter', [
                style({
                    opacity: 0,
                    'margin-top': '20px',
                }),
                animate('100ms', style({ opacity: 1, 'margin-top': '3.5rem' })),
            ]),
            transition(':leave', [
                animate(100),
                style({ opacity: 0, 'margin-top': '20px' }),
            ]),
        ]),
    ],
})
export class SelectComponent
    extends BaseInputComponent
    implements AfterContentInit
{
    customInputs = viewChildren<SelectComponent>(SelectComponent);
    directOptionsDirective = contentChild<OptionsDirective>(OptionsDirective);
    datalistWrapper = contentChild<DataListWrapperComponent<any>>(
        DataListWrapperComponent
    );
    optionsDirective = computed(() => {
        if (this.directOptionsDirective()) {
            return this.directOptionsDirective();
        }
        return this.datalistWrapper()?.optionsDirective();
    });
    dropdownOpen = signal<boolean>(false);
    multiple = input<boolean>(false);
    selectService = inject(SelectService);
    selectedOptionsChange = computed(() =>
        this.optionsDirective()?.selectedOptions()
    );

    constructor(private injector: Injector) {
        super();

        effect(
            () => {
                this.optionsDirective()?.register(this);
            },
            { allowSignalWrites: true }
        );
        effect(
            () => {
                this.datalistWrapper()?.register(this);
            },
            { allowSignalWrites: true }
        );
    }
    @HostListener('document:click', ['$event'])
    handleOutsideClick(event: MouseEvent): void {
        this.dropdownOpen.set(false);
        if (this.selectService.dropdownOpen() === this) {
            this.dropdownOpen.update((old) => !old);
            this.selectService.notifyDropdownOpen(null);
        }
    }
    ngAfterContentInit(): void {
        effect(
            () => {
                if (this.optionsDirective()) {
                    this.optionChange(
                        this.optionsDirective()!.selectedOptions()!
                    );
                }
            },
            { injector: this.injector, allowSignalWrites: true }
        );
    }

    toggleDropdown(event: any) {
        if (this.dropdownOpen()) {
            this.selectService.notifyDropdownOpen(null);
            return;
        }
        this.selectService.notifyDropdownOpen(this);
    }

    optionChange(items: any[]) {
        this.internalValue.set(items);
        this.onChange(this.multiple() ? items : items[0] ?? null);
    }
    removeItem(item: any) {
        this.optionsDirective()?.removeOption(item);
    }

    override writeValue(value: any) {
        if (!value) {
            value = [];
        }
        if (!Array.isArray(value)) {
            value = [value];
        }
        this.internalValue.set(value);
        this.optionsDirective()?.writeSelectedOption(value);
    }
}
