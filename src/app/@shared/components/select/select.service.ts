import { Injectable, signal } from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export class SelectService<T> {
    dropdownOpen = signal<T | null>(null);

    notifyDropdownOpen(component: T) {
        this.dropdownOpen.set(component);
    }
}
