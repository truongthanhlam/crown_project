import {
    Component,
    importProvidersFrom,
    provideZoneChangeDetection,
} from '@angular/core';
import { RouterModule, RouterOutlet } from '@angular/router';
import { bootstrapApplication } from '@angular/platform-browser';
import { getFirestore, provideFirestore } from '@angular/fire/firestore';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { routes } from './app.routes';
import { initializeApp, provideFirebaseApp } from '@angular/fire/app';
import { appConfig } from './app.config';

@Component({
    selector: 'app-root',
    standalone: true,
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    imports: [RouterOutlet],
})
export class AppComponent {
    title = 'crown';

    static bootstrap() {
        return bootstrapApplication(this, appConfig).catch((err) =>
            console.log(err)
        );
    }
}
