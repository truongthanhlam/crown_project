import { Component, signal } from '@angular/core';
import { materials } from '../data-access/models/material.model';
import { NgClass } from '@angular/common';

@Component({
    selector: 'app-material',
    standalone: true,
    imports: [NgClass],
    templateUrl: './material.component.html',
    styleUrl: './material.component.scss',
})
export default class MaterialComponent {
    materials = signal(materials);
}
