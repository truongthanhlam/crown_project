export type MaterialModel = {
    title: string;
    des: string[];
    image: string;
};

export const materials: MaterialModel[] = [
    {
        title: 'VÀNG TÂY',
        des: [
            'Vàng là một trong những kim loại quý không bị oxi hoá hay đổi màu theo thời gian. Do đặc tính của vàng là mềm, khó tạo hình, độ bền không cao, các sản phẩm nữ trang thường sử dụng các hợp kim của vàng như vàng 10k, 14k và 18k.',
            '“K” là viết tắt của từ Karat – đơn vị đo hàm lượng vàng. Các con số 10 – 14 – 18 và 24 chính là số phần vàng có trong sản phẩm.',
        ],
        image: 'assets/material/material_1.jpg',
    },
    {
        title: 'PLATINUM',
        des: [
            'Platinum (bạch kim) thuộc nhóm kim loại quý, hiếm, khó khai thác gấp nhiều lần so với vàng. Platinum là một trong những dòng kim loại đắt nhất (đắt hơn vàng) vì có độ tinh khiết tự nhiên không pha tạp chất và khó bị ăn mòn.',
        ],
        image: 'assets/material/material_2.jpg',
    },
    {
        title: 'PHA LÊ',
        des: [
            'Pha lê có thành phần chính là sillic kali và có thêm một chút oxit chì 2 cùng với oxit bari trong quá trình chế tạo.',
            'Đá pha lê của Crown là loại đá pha lê cao cấp nhập khẩu được làm từ công nghệ cắt giác đẹp và hoàn hảo nhất.',
        ],
        image: 'assets/material/material_3.jpg',
    },
    {
        title: 'ĐÁ QUÝ / BÁN QUÝ',
        des: [
            'Kim cương, ruby, sapphire, emerald được gọi là đá quý.',
            'Ở bất cứ nền văn hoá nào từ Đông sang Tây, các loại đá quý và bán quý cũng được quan niệm là có khả năng kết hợp năng lượng của thiên nhiên với năng lượng của con người, đem lại cho người chủ sở hữu nó những điều tích cực, may mắn trong tinh thần, cảm xúc hay thể chất.',
            'Đá quý và bán quý tại Crown là đá thiên nhiên 100% được nhập khẩu từ Mỹ, Ấn Độ, hoặc Myanmar.',
        ],
        image: 'assets/material/material_4.jpg',
    },
    {
        title: 'KIM CƯƠNG',
        des: [
            'Kim cương là một tinh thể không màu được sản sinh ra từ cacbon nguyên chất trong tự nhiên. Kim cương được xem là vật liệu tự nhiên cứng nhất trên trái đất (độ cứng 10 trên thang Mohs)',
            'Kim cương được Crown chọn lọc kỹ càng và nhập khẩu từ những nguồn uy tín. Kim cương có kích thước từ 3mm trở lên luôn đi kèm kiểm định của Trung tâm Đá quý.',
        ],
        image: 'assets/material/material_5.jpg',
    },
    {
        title: 'KIM CƯƠNG THÔ',
        des: [
            'Kim cương thô là phần kim cương có những đốm trắng, đen, xám trộn lẫn vô cùng độc đáo.',
            'Mới được biết đến và trở thành một “hiện tượng”, mỗi viên kim cương thô mang một vẻ đẹp lạ, phần màu đục và cá tính khiến cho kim cương thô trở nên độc nhất vô nhị.',
            'Mỗi viên kim cương thô của Crown đều được chọn lọc kỹ càng và nhập khẩu từ những nguồn uy tín.',
        ],
        image: 'assets/material/material_6.jpg',
    },
    {
        title: 'NGỌC TRAI',
        des: [
            'Ngọc trai là loại ngọc quý duy nhất được tạo ra bởi vật thể sống. Ngọc trai được đánh giá cao bởi hình dạng, kích thước, màu sắc và độ bóng của chúng. Crown hiện có 2 dòng ngọc trai tự nhiên là ngọc trai nước ngọt (freshwater pearl) và ngọc trai nước mặn (akoya pearl) được tuyển chọn kĩ càng từ những nguồn sản xuất ngọc trai uy tín.',
        ],
        image: 'assets/material/material_7.jpg',
    },
    {
        title: 'MOISSANITE',
        des: [
            'Moissanite là loại đá tạo ra tiêu chuẩn mới trong ngành trang sức bởi moissanite mang đẹp rực rỡ, có độ cứng và bền bỉ không thua gì kim cương tự nhiên. Ngoài ra, quá trình sản xuất moissanite không gây ảnh hưởng xấu tới môi trường. Moissanite là lựa chọn tuyệt vời chi phí rất thấp hơn rất nhiều so với một viên kim cương kích cỡ tương ứng. Với kĩ thuật khoa học tân tiến, chúng được tạo ra kì công đảm bảo tính chất vật lý và tính chất quang học của viên đá tương tự như kim cương tự nhiên.',
            'Crown nhập khẩu moissanite trực tiếp từ Hong Kong, các thông số của viên đá được kiểm định bởi GRA với độ hoàn thiện cao nhất. Giống như Kim cương tự nhiên, tất cả đá moissanite từ 4 ly trở lên đều có chứng nhận và ID riêng.',
        ],
        image: 'assets/material/material_8.jpg',
    },
    {
        title: 'VÀNG BỌC',
        des: [
            'Vàng bọc là một nguyên liệu cao cấp được nhập khẩu từ Mỹ, có lớp vàng 14k dày bọc lấy lõi hợp kim bên trong. Lớp vàng 14k bên ngoài được liên kết với lõi hợp kim bằng phương pháp nhiệt độ và áp suất.',
            'Theo luật của Mỹ và theo thông lệ quốc tế, vàng bọc phải có 1/20 trọng lượng là vàng tây. Vàng bọc KHÔNG phải là vàng mạ, trang sức vàng bọc có độ bền cao hơn nhiều so với trang sức mạ vàng. Bạn có thể tìm hiểu thêm cấu trúc và đặc tính của vàng bọc tại đây.',
        ],
        image: 'assets/material/material_9.jpg',
    },
    {
        title: 'KIM CƯƠNG GHI (GREY DIAMOND)',
        des: [
            'Viên kim cương xám với sự không hoàn hảo sẵn có mang tới cho viên kim cương một vẻ đẹp thú vị, độc đáo. Sự không hoàn hảo tạo ra một làn sương đẹp tuyệt tôn lên vẻ đẹp nguyên thuỷ vốn dĩ của viên đá. Màu sắc trung tính phù hợp với sự tối giản trong tất cả phong cách chính vì vậy mà loại kim cương này ngày càng phổ biến và được ưa chuộng rộng rãi trên thế giới.',
            'Bạn có thể sở hữu được một món trang sức kim cương quý giá mà số tiền bỏ ra thấp hơn rất nhiều so với kim cương tự nhiên trắng.',
        ],
        image: 'assets/material/material_10.jpg',
    },
    {
        title: 'KIM CƯƠNG NÂU (CHAMPAGNE DIAMOND)',
        des: [
            'Kim cương champagne được hình thành với các dấu vết của nitơ trong tinh thể, tạo ra màu sắc đặc trưng của chúng. Từ khi được khám phá, kim cương chưa bao giờ rời khỏi vị trí đầu tiên trong các loại khoáng sản quý hiếm và đẹp nhất hành tinh. Đặc biệt với loại kim cương màu bởi độ quý hiếm của nó. Cũng giống kim cương ghi, kim cương nâu có giá thành vô cùng ‘mềm mại’ so với giá trị của nó đem đến.',
        ],
        image: 'assets/material/material_11.jpg',
    },
];
