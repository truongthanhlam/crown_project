import { Timestamp } from 'firebase/firestore';
import { CategoryModel } from './category.model';

export type DisplayedProductListModel = {
    id?: string | null;
    name: string;
    slug: string;
    description: string;
    thumbnail: string;
    type: 'category' | 'custom';
    category?: CategoryModel;
    productIds: string[];
    createdDate: Timestamp;
    active: boolean;
};
