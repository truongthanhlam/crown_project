export interface Term {
    label: string;
    value: string;
}

export const TermsAndCondition: Term[] = [
    {
        label: 'Giới thiệu',
        value: 'Chào mừng bạn đến với Crownhanoi.com Chúng tôi là Công ty Cổ phần Crownhanoi đã được đăng ký chính thức với Sở Kế hoạch và Đầu tư thành phố Hà Nội (sau đây được gọi là “Crown” hoặc “chúng tôi”) Khi quý khách hàng truy cập vào trang website của chúng tôi có nghĩa là quý khách đồng ý với các điều khoản này. Crown có quyền thay đổi, chỉnh sửa, thêm hoặc lược bỏ bất kỳ phần nào trong Điều khoản mua bán hàng hóa này, vào bất cứ lúc nào. Các thay đổi có hiệu lực ngay khi được đăng trên trang web mà không cần thông báo trước. Và khi quý khách tiếp tục sử dụng trang web, sau khi các thay đổi về Điều khoản này được đăng tải, có nghĩa là quý khách chấp nhận với những thay đổi đó. Quý khách hàng vui lòng kiểm tra thường xuyên để cập nhật những thay đổi của chúng .',
    },
    {
        label: 'Hướng dẫn sử dụng website',
        value: '– Khi vào website của chúng tôi, khách hàng đảm bảo có đầy đủ năng lực hành vi dân sự để thực hiện các giao dịch mua bán hàng hóa theo quy định hiện hành của pháp luật Việt Nam. – Crown sẽ cấp một tài khoản (Account) sử dụng để khách hàng có thể mua sắm trên website Crownhanoi.com trong khuôn khổ Điều khoản và Điều kiện sử dụng đã đề ra. – Quý khách hàng sẽ phải đăng ký tài khoản với thông tin xác thực về bản thân và phải cập nhật nếu có bất kỳ thay đổi nào. Mỗi người truy cập phải có trách nhiệm với mật khẩu, tài khoản và hoạt động của mình trên web. Công ty Crownhanoi không chịu trách nhiệm cho bất kỳ thiệt hại hoặc tổn thất nào cho việc hồ sơ của bạn bị sử dụng một cách trái phép. – Nghiêm cấm sử dụng bất kỳ phần nào của trang web này với mục đích thương mại hoặc nhân danh bất kỳ đối tác thứ ba nào nếu không được chúng tôi cho phép bằng văn bản. Nếu vi phạm bất cứ điều nào trong đây, chúng tôi sẽ hủy tài khoản của khách mà không cần báo trước. – Trong suốt quá trình đăng ký, quý khách đồng ý nhận email quảng cáo từ website. Nếu không muốn tiếp tục nhận mail, quý khách có thể từ chối bằng cách …….',
    },
    {
        label: 'Ý kiến của khách hàng',
        value: 'Tất cả nội dung hiển thị và đăng tải trên Website, bao gồm nhưng không giới hạn ý kiến, nhận xét đóng góp của khách hàng đều là tài sản của Crown. Nếu chúng tôi phát hiện bất kỳ thông tin giả mạo, vu khống nào, chúng tôi có quyền ngay lập tức khóa tài khoản của khách hàng và/hoặc áp dụng các biện pháp khác theo quy định của pháp luật Việt Nam.',
    },
    {
        label: 'Đặt hàng và xác nhận đơn hàng',
        value: '– Crown có quyền từ chối hoặc hủy đơn hàng của quý khách vì bất kỳ lý do gì liên quan đến lỗi kỹ thuật, hệ thống một cách khách quan. – Chúng tôi cam kết sẽ cung cấp thông tin giá cả chính xác nhất cho người tiêu dùng. Tuy nhiên, đôi lúc vẫn có sai sót xảy ra, ví dụ như trường hợp giá sản phẩm không hiển thị chính xác trên trang web hoặc sai giá, tùy theo từng trường hợp chúng tôi sẽ liên hệ hướng dẫn hoặc thông báo hủy đơn hàng đó cho quý khách. ',
    },
    {
        label: 'Thương hiệu và bản quyền',
        value: 'Mọi quyền sở hữu trí tuệ (đã đăng ký hoặc chưa đăng ký), nội dung thông tin và tất cả các thiết kế, văn bản, đồ họa, phần mềm, hình ảnh, video, âm nhạc, âm thanh, biên dịch phần mềm, mã nguồn và phần mềm cơ bản đều là tài sản của Crown. Toàn bộ nội dung của trang web được bảo vệ bởi luật bản quyền của Việt Nam và các công ước quốc tế. Bản quyền đã được bảo lưu.',
    },
    {
        label: 'Quyền pháp lý',
        value: 'Các điều kiện, điều khoản và nội dung của trang web này được điều chỉnh bởi luật pháp Việt Nam và Tòa án có thẩm quyền tại Việt Nam sẽ giải quyết bất kỳ tranh chấp nào phát sinh từ việc sử dụng trái phép trang web này.',
    },
    {
        label: 'Quy định về bảo mật',
        value: '– Trang web của chúng tôi coi trọng việc bảo mật thông tin và sử dụng các biện pháp tốt nhất bảo vệ thông tin và việc thanh toán của quý khách. Thông tin của quý khách trong quá trình thanh toán sẽ được mã hóa để đảm bảo an toàn. Sau khi quý khách hoàn thành quá trình đặt hàng, quý khách sẽ thoát khỏi chế độ an toàn. – Quý khách không được sử dụng bất kỳ chương trình, công cụ hay hình thức nào khác để can thiệp vào hệ thống hay làm thay đổi cấu trúc dữ liệu. Trang web cũng nghiêm cấm việc phát tán, truyền bá hay cổ vũ cho bất kỳ hoạt động nào nhằm can thiệp, phá hoại hay xâm nhập vào dữ liệu của hệ thống. Cá nhân hay tổ chức vi phạm sẽ bị tước bỏ mọi quyền lợi cũng như sẽ bị truy tố trước pháp luật nếu cần thiết. – Mọi thông tin giao dịch sẽ được bảo mật ngoại trừ trong trường hợp cơ quan pháp luật yêu cầu.',
    },
    {
        label: 'Quy trình mua hàng và thanh toán',
        value: 'Khi có nhu cầu mua hàng, sử dụng dịch vụ trên Crownhanoi.com, bạn sẽ thực hiện theo các bước sau đây: • Bước 1: Tìm kiếm, tham khảo thông tin sản phẩm, dịch vụ trên Crown mà bạn đang quan tâm, sau đó, bạn có thể lựa chọn sản phẩm thích hợp. • Bước 2: Dựa trên thông tin tham khảo, bạn đưa ra quyết định đặt hàng trực tuyến bằng cách nhấn vào nút thêm vào giỏ hàng. • Bước 3: Bạn chọn hình thức thanh toán mong muốn, bao gồm thanh toán bằng thẻ thanh toán quốc tế (Visa, Master card..), thẻ ATM nội địa, thông qua thanh toán trực tuyến hoặc thanh toán bằng khoản ví điện tử Ngân lượng sau đó thực hiện thanh toán trực tuyến. • Bước 4: Thanh toán thành công và bạn sẽ nhận được email thông báo thông tin đơn hàng. *Lưu ý: Trong mọi trường hợp, Crown chỉ bắt đầu tiến hành đơn hàng sau khi nhận thanh toán 100% giá trị đơn hàng',
    },
    {
        label: 'Chế tác và vận chuyển',
        value: 'Crown sẽ tiến hành chế tác sản phẩm sau khi nhận được thanh toán 100% giá trị đơn hàng của quý khách. Sản phẩm sẽ được chế tác trong 10 ngày (không tính các ngày lễ tết chính thức của Việt Nam). Thời gian vận chuyển đến quý khách kể từ khi chế tác xong là 1-3 ngày làm việc với địa chỉ trong nội thành Hà Nội, 2-7 ngày làm việc với các địa chỉ ngoại thành. Crown miễn phí vận chuyển toàn quốc với thời gian tiêu chuẩn như trên, trừ trường hợp cần vận chuyển gấp thì khách hàng sẽ thanh toán tiền vận chuyển cho Crown.',
    },
    {
        label: 'Nhận hàng và kiểm tra',
        value: '– Quý khách khi nhận hàng phải kiểm tra các yếu tổ bên ngoài của kiện hàng, bao gồm: hộp còn nguyên vẹn không rách, nát, tem niêm phong chưa bị xé/tách rời, nhãn mác kiện hàng còn nguyên vẹn. – Nếu phát hiện kiện hàng có dấu hiệu móp méo, không còn nguyên vẹn hoặc sai thông tin người nhận, quý khách vui lòng giữ nguyên hiện trạng và chụp ảnh một cách chi tiết nhất về kiện hàng, sau đó gửi ảnh cho Crown để Crown hỗ trợ xử lí và làm căn cứ trong các trường hợp cần thiết.',
    },
    {
        label: 'Chính sách giải quyết khiếu nại',
        value: 'Quy trình khiếu nại thực hiện theo các bước sau: Bước 1: Khách hàng khiếu nại về hàng hóa, dịch vụ của Crown thực hiện qua các hình thức sau: – Gửi thư điện tử đến địa chỉ email: cskh.crownhanoi@gmail.com; hoặc – Người mua gửi khiếu nại tại địa chỉ: Cơ sở 1: Số 119, Kim Mã, HN Cơ sở 2: Số 42, Đặng Thị Nhu, Quận 1, HCM Bước 2: Bộ phận chăm sóc khách hàng của Crown sẽ tiếp nhận các khiếu nại, liên hệ làm rõ các yêu cầu của Khách hàng trong thời gian sớm nhất có thể và không quá 3 ngày làm việc, kể từ ngày nhận được yêu cầu. Tùy theo tính chất và mức độ của sự việc, Crown sẽ có những biện pháp cụ thể để hỗ trợ Khách hàng giải quyết khiếu nại, tranh chấp. Bước 3: Crown có thể yêu cầu Khách hàng cung cấp các thông tin, bằng chứng liên quan đến giao dịch, sản phẩm để xác minh, làm rõ vụ việc và có hướng xử lý thích hợp. Bước 4: Trong trường hợp Crown đã nỗ lực giải quyết khiếu nại, tranh chấp nhưng sự việc vượt quá khả năng và thẩm quyền của Crown, Crown sẽ yêu cầu Khách hàng đưa vụ việc ra cơ quan Nhà nước có thẩm quyền giải quyết theo quy định của pháp luật. Crown tôn trọng và nghiêm túc thực hiện các quy định của pháp luật về bảo vệ quyền lợi của Khách hàng (người tiêu dùng). Bất kỳ tranh cãi, khiếu nại hoặc tranh chấp phát sinh từ hoặc liên quan đến giao dịch tại Website hoặc các Quy định và Điều kiện này đều sẽ được giải quyết bằng hình thức thương lượng, hòa giải, trọng tài và/hoặc Tòa án theo Luật Bảo vệ quyền lợi người tiêu dùng năm 2010.',
    },
    {
        label: 'Giá cả',
        value: 'Giá cả sản phẩm được niêm yết tại Website là giá bán cuối cùng đã bao gồm thuế Giá trị gia tăng (VAT). Giá của sản phẩm có thể thay đổi tùy thời điểm và chương trình khuyến mãi kèm theo.',
    },
    {
        label: 'Thông tin sản phẩm',
        value: 'Chúng tôi đã cố gắng hết sức để hiển thị chính xác sản phẩm về mặt hình ảnh cũng như ngôn ngữ. Tuy nhiên, Crown không thể đảm bảo rằng tất cả các thiết bị sẽ hiển thị màu sắc chính xác, đặc biệt là đối với các sản phẩm đá tự nhiên. Nếu bạn có bất kỳ câu hỏi nào về màu sắc hoặc hình dạng của sản phẩm, vui lòng liên hệ với Crown qua facebook hoặc instagram. Do sản phẩm được làm thủ công tỉ mỉ nên sẽ có sai số nhất định so với mô tả sản phẩm sẽ ít hơn 5%. Trong trường hợp sản phẩm quý khách nhận được không đúng như mô tả thông tin sản phẩm trên Website, vui lòng gửi thông tin ngay cho Crown và đảm bảo sản phẩm trong tình trạng chưa sử dụng để được hỗ trợ.',
    },
];
