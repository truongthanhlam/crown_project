export type StoreModel = {
    id?: string;
    name: string;
    address: string;
    phone: string;
    thumbnail: string;
    active: boolean;
    createdDate: Date;
};
