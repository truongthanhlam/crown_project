import { CategoryModel } from './category.model';

export type BlogModel = {
    id?: string;
    title: string;
    slug: string;
    categories: CategoryModel[];
    thumbnail: string;
    active: boolean;
    content: string;
    createdDate: Date;
};
