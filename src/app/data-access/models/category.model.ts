export interface CategoryModel {
    id?: string;
    viName: string;
    enName?: string;
    description?: string;
    parent?: CategoryModel;
    level: number;
    priority: number;
    image: string;
    createdDate: Date;
}
export const categoryStringify = (item: CategoryModel) => item.viName;
