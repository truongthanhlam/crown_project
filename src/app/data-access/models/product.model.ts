import { CategoryModel } from './category.model';

export type VariationModel = {
    ids: string[];
    name: string;
    price: number;
    sku: string;
};

type SeoModel = {
    metaTitle: string;
    metaDes: string;
    metaImage: string;
    isIndex: boolean;
    isNoFollow: boolean;
    isNoArchive: boolean;
    isNoImageIndex: boolean;
    isNoSnipped: boolean;
    isMaxSnipped: boolean;
    isMaxVideoReview: boolean;
    isMaxImageReview: boolean;
    maxSnipped: number;
    maxVideoReview: number;
    maxImageReview: string;
};
export type ProductModel = {
    id?: string;
    name: string;
    description: string;
    detail: string;
    category: CategoryModel;
    type: CategoryModel;
    sku: string;
    displayedPrice?: string;
    searchKeys: string[];
    discountType: CategoryModel;
    discountAmount: number;
    taxAmount: number;
    taxCalculation: CategoryModel;
    shippingCost: number;
    shippingMultiply: boolean;
    containSubProducts: boolean;
    attribute: CategoryModel[];
    thumbnail: string;
    photos: string[];
    video: string;
    seo: SeoModel;
    attributeValue: CategoryModel[][];
    variation: VariationModel[];
    status?: boolean;
    createdDate: Date;
};

export type ProductQueryModel = {
    keyword?: String;
    category?: CategoryModel;
    status?: String;
};
