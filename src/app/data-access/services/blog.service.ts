import { Injectable } from '@angular/core';
import { FirestoreBaseService } from './firestore-base.service';
import { BlogModel } from '../models/blog.model';
import { where } from 'firebase/firestore';
import { map } from 'rxjs';
import { DisplayedProductListModel } from '../models/displayed-product-list.model';

@Injectable({
    providedIn: 'root',
})
export class BlogService extends FirestoreBaseService<BlogModel> {
    constructor() {
        super('Blog');
    }

    getBySlug(slug: string) {
        const q = this.buildQuery(where('slug', '==', slug));
        return this.queryItems(q).pipe(
            map((list) => {
                return list[0] as BlogModel | undefined;
            })
        );
    }
}
