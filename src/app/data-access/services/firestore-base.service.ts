import { inject, Injectable } from '@angular/core';
import {
    addDoc,
    collection,
    collectionData,
    deleteDoc,
    doc,
    Firestore,
    updateDoc,
} from '@angular/fire/firestore';
import {
    getDoc,
    Query,
    query,
    QueryConstraint,
    where,
} from 'firebase/firestore';
import { Observable } from 'rxjs';

export interface FirestoreResponse<T> {
    data: T;
    id?: string;
}

export class FirestoreBaseService<T> {
    firestore = inject(Firestore);

    collectionName: string = '';

    constructor(collectionName: string) {
        this.collectionName = collectionName;
    }

    buildQuery(...queryConstraints: QueryConstraint[]): Query {
        const baseQ = query(
            collection(this.firestore, this.collectionName),
            ...queryConstraints
        );

        return baseQ;
    }
    queryItems(q: Query): Observable<T[]> {
        return collectionData(q, { idField: 'id' });
    }

    async getItemById(id: string): Promise<T> {
        const itemsCollection = collection(this.firestore, this.collectionName);
        const docRef = await getDoc(doc(itemsCollection, id));
        return {
            ...docRef.data(),
            id: docRef.id,
        } as T;
    }
    getItems(): Observable<T[]> {
        const q = this.buildQuery(where('active', '==', true));

        return collectionData(q, { idField: 'id' });
    }

    addItem(data: T): Promise<FirestoreResponse<T>> {
        return addDoc(
            collection(this.firestore, this.collectionName),
            data as { [x: string]: any }
        ).then((ref: any) => ({ data, id: ref.id }));
    }

    deleteItem(documentId: string): Promise<void> {
        return deleteDoc(doc(this.firestore, this.collectionName, documentId));
    }

    updateItem(documentId: string, data: Partial<T>): Promise<void> {
        return updateDoc(
            doc(this.firestore, this.collectionName, documentId),
            data
        );
    }
}
