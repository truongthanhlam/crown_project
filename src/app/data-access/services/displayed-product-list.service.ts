import { Injectable } from '@angular/core';
import { FirestoreBaseService } from './firestore-base.service';
import { DisplayedProductListModel } from '../models/displayed-product-list.model';
import { where } from 'firebase/firestore';
import { map } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class DisplayedProductListService extends FirestoreBaseService<DisplayedProductListModel> {
    constructor() {
        super('DisplayedProductList');
    }

    getBySlug(slug: string) {
        const q = this.buildQuery(where('slug', '==', slug));
        return this.queryItems(q).pipe(
            map((list) => {
                return list[0] as DisplayedProductListModel | undefined;
            })
        );
    }
}
