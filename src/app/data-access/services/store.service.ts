import { Injectable } from '@angular/core';
import { and, or, query } from '@angular/fire/firestore';
import { where } from 'firebase/firestore';
import { FirestoreBaseService } from './firestore-base.service';
import { StoreModel } from '../models/store.model';

@Injectable({
    providedIn: 'root',
})
export class StoreService extends FirestoreBaseService<StoreModel> {
    constructor() {
        super('Store');
    }

    getByKeyword(keyword?: string | null) {
        let q = this.buildQuery();
        if (keyword) {
            q = query(
                q,
                or(
                    and(
                        where('name', '>=', keyword),
                        where('name', '<=', keyword + 'z')
                    ),
                    and(
                        where('address', '>=', keyword),
                        where('address', '<=', keyword + 'z')
                    )
                )
            );
        }
        return this.queryItems(q);
    }
}
