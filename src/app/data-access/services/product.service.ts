import { Injectable } from '@angular/core';
import { FirestoreBaseService } from './firestore-base.service';
import { ProductModel, VariationModel } from '../models/product.model';
import { collection, getDoc, doc } from 'firebase/firestore';
import { CategoryModel } from '../models/category.model';
import { formatCurrency } from '../../@shared/util/string.util';

@Injectable({
    providedIn: 'root',
})
export class ProductService extends FirestoreBaseService<ProductModel> {
    constructor() {
        super('Product');
    }

    // TODO: This is workaround for nested array not supported issue in firestore, this should been removed in future
    override async getItemById(id: string): Promise<ProductModel> {
        const itemsCollection = collection(this.firestore, this.collectionName);
        const productDoc = await getDoc(doc(itemsCollection, id));

        const revertedArray: CategoryModel[][] = Object.keys(
            productDoc.data()!['attributeValue']
        )
            .sort((a, b) => Number(a) - Number(b))
            .map((key) => productDoc.data()!['attributeValue'][key]);

        return {
            ...productDoc.data(),
            id: productDoc.id,
            attributeValue: revertedArray,
            displayedPrice: this.calculateDisplayedPrice(
                productDoc.data()!['variation']
            ),
        } as ProductModel;
    }

    getItemByIds(productIds: string[]) {
        return Promise.all(productIds.map((id) => this.getItemById(id)));
    }

    calculateDisplayedPrice(variation: VariationModel[] | null | undefined) {
        if (!variation) {
            return null;
        }
        if (variation.length === 1) {
            return formatCurrency(variation[0].price.toString());
        }
        const size = variation.length;
        variation = variation.sort((a, b) => a.price - b.price);
        return `${formatCurrency(
            variation[0].price.toString()
        )} - ${formatCurrency(variation[size - 1].price.toString())}`;
    }
}
