import { Component, inject } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
    selector: 'app-ring-size-guide',
    standalone: true,
    imports: [],
    templateUrl: './ring-size-guide.component.html',
    styleUrl: './ring-size-guide.component.scss',
})
export default class RingSizeGuideComponent {
    sanitizer = inject(DomSanitizer);
    safeUrl: SafeResourceUrl;

    constructor() {
        this.safeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(
            `https://www.youtube.com/embed/uC_Yp53Ih70?feature=oembed`
        );
    }
}
