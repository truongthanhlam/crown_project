import { Component, HostListener, inject, signal } from '@angular/core';
import { Router, RouterLink, RouterOutlet } from '@angular/router';
import { DisplayedProductListService } from '../data-access/services/displayed-product-list.service';
import { toSignal } from '@angular/core/rxjs-interop';
import { NgIcon, provideIcons } from '@ng-icons/core';
import {
    matArrowDownward,
    matArrowDropDown,
    matKeyboardArrowDown,
} from '@ng-icons/material-icons/baseline';
import { FormsModule, NgModel } from '@angular/forms';

@Component({
    selector: 'app-layout',
    standalone: true,
    imports: [RouterOutlet, RouterLink, NgIcon, FormsModule],
    templateUrl: './layout.component.html',
    styleUrl: './layout.component.scss',
    providers: [provideIcons({ matKeyboardArrowDown })],
})
export default class LayoutComponent {
    displayedProductListService = inject(DisplayedProductListService);
    sideBarOpen = signal(false);

    list = toSignal(this.displayedProductListService.getItems());
    router = inject(Router);
    constructor() {}

    ngOnInit(): void {}

    navigate(path: string) {
        this.router.navigate(['/' + path]);
    }

    closeDropdown(dropdown: HTMLDivElement) {
        const elem = document.activeElement as HTMLElement;
        if (elem) {
            elem.blur();
        }
        dropdown.blur();
    }
    closeSideBar() {
        this.sideBarOpen.set(false);
    }
}
