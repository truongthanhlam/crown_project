import { Route } from '@angular/router';

const layoutRoutes: Route[] = [
    {
        path: 'home',
        loadComponent: () => import('../home/home.component'),
    },
    {
        path: 'contact',
        loadComponent: () => import('../contact/contact.component'),
    },
    {
        path: 'about-us',
        loadComponent: () => import('../about-us/about-us.component'),
    },
    {
        path: 'warranty',
        loadComponent: () => import('../warranty-home/warranty-home.component'),
    },
    {
        path: 'warranty-1',
        loadComponent: () => import('../warranty-1/warranty-1.component'),
    },
    {
        path: 'warranty-2',
        loadComponent: () => import('../warranty-2/warranty-2.component'),
    },
    {
        path: 'membership',
        loadComponent: () => import('../membership/membership.component'),
    },
    {
        path: 'ring-size-guide',
        loadComponent: () =>
            import('../ring-size-guide/ring-size-guide.component'),
    },
    {
        path: 'stores',
        loadComponent: () => import('../stores/stores.component'),
    },
    {
        path: 'blog',
        loadComponent: () => import('../blog/blog.component'),
    },
    {
        path: 'blog/:slug',
        loadComponent: () => import('../blog-detail/blog-detail.component'),
    },
    {
        path: 'terms-and-conditions',
        loadComponent: () =>
            import('../terms-and-conditions/terms-and-conditions.component'),
    },
    {
        path: 'material',
        loadComponent: () => import('../material/material.component'),
    },
    {
        path: ':type',
        loadComponent: () => import('../nhan-nu/nhan-nu.component'),
    },
    {
        path: ':type/:slug',
        loadComponent: () => import('../detail/detail.component'),
    },
    {
        path: '**',
        redirectTo: '/home',
        pathMatch: 'full',
    },
];

export default layoutRoutes;
