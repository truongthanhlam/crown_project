import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
    selector: '[appImageZoomPreview]',
    standalone: true,
})
export class ImageZoomPreviewDirective {
    constructor(private el: ElementRef) {}

    @HostListener('mouseenter') onMouseEnter() {
        this.el.nativeElement.style.transform = 'scale(1.7)';
    }

    @HostListener('mouseleave') onMouseLeave() {
        this.el.nativeElement.style.transform = 'scale(1)';
    }

    @HostListener('mousemove', ['$event'])
    onMouseMove(e: MouseEvent) {
        const ele = this.el.nativeElement as HTMLElement;
        const rect = ele.getBoundingClientRect();

        this.el.nativeElement.style['transform-origin'] =
            ((e.clientX - rect.left) / rect.width) * 100 +
            '% ' +
            ((e.clientY - rect.top) / rect.height) * 100 +
            '%';
    }
}
