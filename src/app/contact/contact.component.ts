import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-contact',
    templateUrl: './contact.component.html',
    styleUrls: ['./contact.component.scss'],
    standalone: true,
})
export default class ContactComponent implements OnInit {
    contacts: any[] = [
        {
            src: 'https://crownhanoi.store/wp-content/uploads/2020/08/467C9900-9FF0-41CE-9318-6FA7361B1374-E2AEDA93-F99B-4FB6-B82C-E1BCEB144E76-2048x2048.jpg',
            provine: 'HANOI – BA DINH',
            district: '261 Giảng Võ, Quận Ba Đình',
            phone: '0123456789',
        },
        {
            src: 'https://crownhanoi.store/wp-content/uploads/2020/08/E1F43328-87CB-4446-A29E-04DFB7C8EE0C-3DE3416D-B950-474C-8477-521F8DB32759-2048x2048.jpg',
            provine: 'TP.HO CHI MINH – QUAN I',
            district: '42 Đặng Thị Nhu, Quận 1',
            phone: '0123456789',
        },
        {
            src: 'https://crownhanoi.store/wp-content/uploads/2020/08/01BFACC7-2751-4E3E-AC79-B1FC72BC453B-1-2048x2048.jpg',
            provine: 'HANOI – HOAN KIEM',
            district: '50A Hàng Bài, Q Hoàn Kiếm',
            phone: '0123456789',
        },
    ];

    constructor() {}

    ngOnInit(): void {}
}
