import { Component, computed, inject } from '@angular/core';
import { toSignal } from '@angular/core/rxjs-interop';
import { StoreService } from '../data-access/services/store.service';

@Component({
    selector: 'app-stores',
    standalone: true,
    imports: [],
    templateUrl: './stores.component.html',
    styleUrl: './stores.component.scss',
})
export default class StoresComponent {
    storeService = inject(StoreService);

    storeList = toSignal(this.storeService.getItems());
    highLightStores = computed(() =>
        this.storeList() && this.storeList()!.length > 2
            ? this.storeList()!.slice(0, 2)
            : []
    );
    otherStores = computed(() =>
        this.storeList() && this.storeList()!.length > 2
            ? this.storeList()!.slice(2)
            : []
    );
}
