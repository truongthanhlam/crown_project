import { Component } from '@angular/core';

@Component({
    selector: 'app-warranty-1',
    standalone: true,
    imports: [],
    templateUrl: './warranty-1.component.html',
    styleUrl: './warranty-1.component.scss',
})
export default class Warranty1Component {}
