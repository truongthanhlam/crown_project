import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Warranty1Component } from './warranty-1.component';

describe('Warranty1Component', () => {
  let component: Warranty1Component;
  let fixture: ComponentFixture<Warranty1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [Warranty1Component]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Warranty1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
