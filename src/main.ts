import { AppComponent } from './app/app.component';

(async () => {
    void AppComponent.bootstrap();
})();
